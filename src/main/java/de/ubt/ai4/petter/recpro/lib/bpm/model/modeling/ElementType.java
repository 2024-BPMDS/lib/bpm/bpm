package de.ubt.ai4.petter.recpro.lib.bpm.model.modeling;

public enum ElementType {
    PROCESS, PROCESS_ACTIVITY, ROLE, PROCESS_MODEL;

    public static ElementType findType(String type) {
        return switch (type) {
            case "PROCESS" -> PROCESS;
            case "PROCESS_MODEL" -> PROCESS_MODEL;
            case "ROLE" -> ROLE;
            default -> PROCESS_ACTIVITY;
        };
    }
}

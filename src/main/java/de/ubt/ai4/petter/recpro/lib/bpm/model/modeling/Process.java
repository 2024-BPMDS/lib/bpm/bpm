package de.ubt.ai4.petter.recpro.lib.bpm.model.modeling;

import de.ubt.ai4.petter.recpro.lib.bpms.modeling.model.BpmsProcess;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Process extends BpmElement {
    private String version;
    private String key;
    private boolean latest;
    private ProcessModel processModel;

    public static Process fromBpmsProcess(BpmsProcess process) {
        Process result = new Process();
        result.setId(process.getId());
        result.setName(process.getName());
        result.setDescription(process.getDescription());
        result.setAttributes(new ArrayList<>());
        result.setElementType(ElementType.PROCESS);
        result.setVersion(process.getVersion());
        result.setKey(process.getKey());
        result.setLatest(process.isLatest());
        result.setProcessModel(ProcessModel.fromBpmsProcessModel(process.getProcessModel()));
        return result;
    }

    public static List<Process> fromBpmsProcesses(List<BpmsProcess> processes) {
        return processes.stream().map(Process::fromBpmsProcess).toList();
    }
}

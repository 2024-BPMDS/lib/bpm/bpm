package de.ubt.ai4.petter.recpro.lib.bpm.model.modeling;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttribute;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BpmElement {
    private String id;
    private String name;
    private String description;
    private List<RecproAttribute> attributes = new ArrayList<>();
    private ElementType elementType;
}

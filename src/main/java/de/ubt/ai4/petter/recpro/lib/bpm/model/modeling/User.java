package de.ubt.ai4.petter.recpro.lib.bpm.model.modeling;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class User {
    private String id;
    private String lastName;
    private String firstName;
    private String description;
    private List<Role> roles = new ArrayList<>();
}
